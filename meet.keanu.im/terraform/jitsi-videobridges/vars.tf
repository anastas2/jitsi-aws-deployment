provider "sops" {}

data "sops_file" "secrets" {
  source_file = "../_vars/secrets.sops.yml"
}

variable "aws_region" {
  type = string
}

locals {
  namespace             = var.namespace
  stage                 = var.stage
  name                  = "jvb"
  aws_region            = var.aws_region
  remote_state_bucket   = "${local.namespace}-${local.stage}-terraform-state"
  tags                  = map("Project", data.sops_file.secrets.data["project_tag"])
  india_region          = data.sops_file.secrets.data["vpc_india_region"]
  us1_region            = data.sops_file.secrets.data["vpc_us1_region"]
  jitsi_vpc_cidr_blocks = data.terraform_remote_state.vpc_satellites.outputs.vpc_all_cidr_blocks
}

