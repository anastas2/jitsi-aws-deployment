output "ssm_playbook" {
  value = module.ssm_setup
}
output "ansible_playbooks_bundle_s3_object" {
  value = aws_s3_bucket_object.ansible_playbooks_bundle
}
output "ansible_playbooks_s3_bucket" {
  value = module.ssm_setup.playbooks_bucket_id
}
output "ansible_playbooks_bundle_md5" {
  value = local.ansible_bundle_md5
}
