terraform {
  required_version = ">= 0.12"

  backend "s3" {
    key = "setup/terraform.tfstate"
  }
  required_providers {
    sops = "~> 0.5"
  }
}

provider "aws" {}

resource "aws_s3_account_public_access_block" "block_public_s3" {
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

module "ssm_setup" {
  source        = "git::https://gitlab.com/guardianproject-ops/terraform-aws-ssm-ansible-setup?ref=v2.0.0"
  namespace     = local.namespace
  stage         = local.stage
  name          = local.name
  force_destroy = true
}


data "archive_file" "ansible_playbooks_bundle" {
  type        = "zip"
  source_dir  = "../../../ansible"
  output_path = "${path.module}/ansible.zip"
}

locals {
  ansible_bundle_md5 = filemd5(data.archive_file.ansible_playbooks_bundle.output_path)
}

resource "aws_s3_bucket_object" "ansible_playbooks_bundle" {
  key    = "jitsi-common/ansible-${local.ansible_bundle_md5}.zip"
  bucket = module.ssm_setup.playbooks_bucket_id
  source = data.archive_file.ansible_playbooks_bundle.output_path
  etag   = local.ansible_bundle_md5
  acl    = "private"
}
