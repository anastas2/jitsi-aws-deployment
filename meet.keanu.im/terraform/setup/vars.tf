provider "sops" {}

data "sops_file" "secrets" {
  source_file = "../_vars/secrets.sops.yml"
}
variable "aws_region" {
  type = string
}

locals {
  namespace  = var.namespace
  stage      = var.stage
  name       = "setup"
  aws_region = var.aws_region
  tags       = map("Project", data.sops_file.secrets.data["project_tag"])
}
