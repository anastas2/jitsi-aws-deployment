terraform {
  required_version = ">= 0.12"

  backend "s3" {
    key = "vpc-main/terraform.tfstate"
  }
  required_providers {
    sops = "~> 0.5"
  }
}

provider "aws" {}

module "vpc" {
  source     = "git::https://github.com/cloudposse/terraform-aws-vpc.git?ref=tags/0.10.0"
  namespace  = local.namespace
  stage      = local.stage
  name       = "vpcmain"
  attributes = [local.aws_region]
  tags       = local.tags
  cidr_block = local.cidr_block
}

locals {
  public_cidr_block  = cidrsubnet(module.vpc.vpc_cidr_block, 1, 0)
  private_cidr_block = cidrsubnet(module.vpc.vpc_cidr_block, 1, 1)
}

module "public_subnets" {
  source            = "git::https://github.com/cloudposse/terraform-aws-named-subnets.git?ref=tags/0.5.0"
  namespace         = local.namespace
  stage             = local.stage
  name              = "public"
  attributes        = [local.aws_region]
  tags              = merge(local.tags, map("Visibility", "Public"))
  subnet_names      = ["jitsi1"]
  vpc_id            = module.vpc.vpc_id
  cidr_block        = local.public_cidr_block
  type              = "public"
  igw_id            = module.vpc.igw_id
  availability_zone = local.subnet_az
  nat_enabled       = false
}

module "cloudwatch_alarms" {
  source                                = "../../../terraform-modules/cloudwatch-alarms"
  namespace                             = local.namespace
  stage                                 = local.stage
  name                                  = "cloudwatch"
  tags                                  = local.tags
  sns_topic_subscription_https_endpoint = local.victorops_cloudwatch_url
}
