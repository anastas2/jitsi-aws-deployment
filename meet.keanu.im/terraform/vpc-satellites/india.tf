locals {
  india_region            = data.sops_file.secrets.data["vpc_india_region"]
  india_cidr_block        = data.sops_file.secrets.data["vpc_india_cidr_block"]
  india_subnet_az         = data.sops_file.secrets.data["vpc_india_subnet_az"]
  india_public_cidr_block = cidrsubnet(module.vpc_india.vpc_cidr_block, 1, 0)
}

module "vpc_india" {
  providers = {
    aws = aws.india
  }
  source     = "git::https://github.com/cloudposse/terraform-aws-vpc.git?ref=tags/0.10.0"
  namespace  = local.namespace
  stage      = local.stage
  name       = local.name
  attributes = [local.india_region]
  tags       = local.tags
  cidr_block = local.india_cidr_block
}

module "public_subnets_india" {
  source = "git::https://github.com/cloudposse/terraform-aws-named-subnets.git?ref=tags/0.5.0"
  providers = {
    aws = aws.india
  }
  namespace         = local.namespace
  stage             = local.stage
  name              = local.name
  attributes        = ["public", local.india_region]
  tags              = merge(local.tags, map("Visibility", "Public"))
  subnet_names      = ["jitsi1"]
  vpc_id            = module.vpc_india.vpc_id
  cidr_block        = local.india_public_cidr_block
  type              = "public"
  igw_id            = module.vpc_india.igw_id
  availability_zone = local.india_subnet_az
  nat_enabled       = false
}

module "cloudwatch_alarms_india" {
  providers = {
    aws = aws.india
  }
  source                                = "../../../terraform-modules/cloudwatch-alarms"
  namespace                             = local.namespace
  stage                                 = local.stage
  name                                  = "cloudwatch"
  tags                                  = local.tags
  sns_topic_subscription_https_endpoint = local.victorops_cloudwatch_url
}
