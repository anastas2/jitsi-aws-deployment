locals {
  us1_region            = data.sops_file.secrets.data["vpc_us1_region"]
  us1_cidr_block        = data.sops_file.secrets.data["vpc_us1_cidr_block"]
  us1_subnet_az         = data.sops_file.secrets.data["vpc_us1_subnet_az"]
  us1_public_cidr_block = cidrsubnet(module.vpc_us1.vpc_cidr_block, 1, 0)
}
module "vpc_us1" {
  providers = {
    aws = aws.us1
  }
  source     = "git::https://github.com/cloudposse/terraform-aws-vpc.git?ref=tags/0.10.0"
  namespace  = local.namespace
  stage      = local.stage
  name       = local.name
  attributes = [local.us1_region]
  tags       = local.tags
  cidr_block = local.us1_cidr_block
}

module "public_subnets_us1" {
  source = "git::https://github.com/cloudposse/terraform-aws-named-subnets.git?ref=tags/0.5.0"
  providers = {
    aws = aws.us1
  }
  namespace         = local.namespace
  stage             = local.stage
  name              = local.name
  attributes        = ["public", local.us1_region]
  tags              = merge(local.tags, map("Visibility", "Public"))
  subnet_names      = ["jitsi1"]
  vpc_id            = module.vpc_us1.vpc_id
  cidr_block        = local.us1_public_cidr_block
  type              = "public"
  igw_id            = module.vpc_us1.igw_id
  availability_zone = local.us1_subnet_az
  nat_enabled       = false
}

module "cloudwatch_alarms_us1" {
  providers = {
    aws = aws.us1
  }
  source                                = "../../../terraform-modules/cloudwatch-alarms"
  namespace                             = local.namespace
  stage                                 = local.stage
  name                                  = "cloudwatch"
  tags                                  = local.tags
  sns_topic_subscription_https_endpoint = local.victorops_cloudwatch_url
}
