output "monitoring_instance" {
  value = module.monitoring_instance
}
output "matrix_alertmanager_lambda" {
  value = module.matrix_alertmanager
}
