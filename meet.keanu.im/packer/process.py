import json
import sys
import os

if not os.path.exists('manifest.json'):
    sys.exit(1)

with open('manifest.json', 'r') as f:
    manifest = json.load(f)


if not "builds" in manifest or len(manifest["builds"]) == 0:
    sys.exit(1)

for build in manifest["builds"]:
    time = build["build_time"]
    ami = build["artifact_id"]
    name = build["name"]
    print(time, ami)
